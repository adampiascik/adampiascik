-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 09 Maj 2017, 17:43
-- Wersja serwera: 5.7.12-log
-- Wersja PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `livevideo`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie`
--

CREATE TABLE `kategorie` (
  `id` int(10) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `kategorie`
--

INSERT INTO `kategorie` (`id`, `name`) VALUES
(1, 'Góry'),
(2, 'Morze'),
(3, 'Jeziora'),
(4, 'Miasta'),
(5, 'Zwierzęta'),
(6, 'Norwegia'),
(7, 'Finlandia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `livevideo`
--

CREATE TABLE `livevideo` (
  `id` int(10) NOT NULL,
  `kategorie_id` int(10) NOT NULL,
  `name` text NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `livevideo`
--

INSERT INTO `livevideo` (`id`, `kategorie_id`, `name`, `url`) VALUES
(1, 2, '\r\nSopot-molo', 'https://player.webcamera.pl/index.php?id=sopot_cam_e1c28f'),
(2, 2, 'Gdańsk-Brzeźno', 'https://player.webcamera.pl/index.php?id=gdansk_cam_77aeaf'),
(3, 2, 'MIĘDZYZDROJE na żywo', 'https://player.webcamera.pl/index.php?id=miedzyzdroje_cam_613997'),
(4, 2, 'KOŁOBRZEG plaża ', 'https://player.webcamera.pl/index.php?id=kolobrzeg_cam_c26ccc'),
(5, 2, '\r\nKołobrzeg molo z głosem', 'https://player.webcamera.pl/index.php?id=nocowanie_cam_a32dd1'),
(6, 1, 'KOŁOBRZEG Marina', 'https://player.webcamera.pl/index.php?id=kolobrzegmarina_cam_967ce0'),
(7, 1, 'Kasprowy - Panorama', 'https://player.webcamera.pl/index.php?id=pkl_cam_72b29d'),
(8, 1, 'Krupówki - Zakopane Live', 'https://player.webcamera.pl/index.php?id=zakopane_cam_cd5b2d_reklamy'),
(9, 2, 'ŁEBA plaża NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=leba_cam_0d3c03'),
(10, 2, 'ŁEBA - plaża', 'https://player.webcamera.pl/index.php?id=leba_cam_863fcf'),
(12, 4, 'Mikołajki Rynek', 'https://player.webcamera.pl/index.php?id=nocowanie_cam_3ed223'),
(13, 4, 'Hotel Gołębiewski - Mikołajki', 'https://player.webcamera.pl/index.php?id=golebiewskimiko_cam_6bb288'),
(14, 3, 'Wilkasy - widok na Jezioro Niegocin', 'https://player.webcamera.pl/index.php?id=skywind_cam_99451e'),
(15, 3, 'widok na Jezioro Solińskie NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=airnet_cam_0a7765'),
(16, 4, 'Wejherowo rynek', 'https://player.webcamera.pl/index.php?id=um_wejherowo_cam_47c4b7'),
(17, 4, 'GDYNIA - Skwer Kościuszki', 'https://player.webcamera.pl/index.php?id=gdynia_cam_b3ccea'),
(18, 2, 'USTKA Panorama', 'https://player.webcamera.pl/index.php?id=ustka_cam_4b0fca'),
(19, 2, 'Ustka - Promenada', 'https://player.webcamera.pl/index.php?id=ustka_cam_fefaff'),
(20, 4, 'Katowice - Spodek', 'https://player.webcamera.pl/index.php?id=katowice_cam_2bcad5'),
(21, 1, 'Międzyzdroje deptak', 'https://player.webcamera.pl/index.php?id=miedzyzdroje_cam_613997'),
(22, 2, 'KĄTY RYBACKIE - plaża', 'https://player.webcamera.pl/index.php?id=katyrybackie_cam_2d3c0c'),
(23, 2, 'Krynica Morska - port jachtowy', 'https://player.webcamera.pl/index.php?id=krynicamorska_cam_b92412'),
(24, 4, 'Malbork widok na zamek ', 'https://player.webcamera.pl/index.php?id=malbork_cam_0ffc99'),
(25, 2, 'STEGNA - plaża NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=stegna_cam_c00d9b'),
(26, 4, 'Gdańsk - widok na Motławę', 'https://player.webcamera.pl/index.php?id=umgdansk_cam_11161c'),
(27, 2, 'Świnoujście - plaża live', 'https://player.webcamera.pl/index.php?id=swinoujscie_cam_7eca1e'),
(28, 1, 'Wrocław Stare Miasto', 'https://player.webcamera.pl/index.php?id=wroclaw_przed_cam_16c68e'),
(29, 1, 'LĄDEK ZDRÓJ - góra', 'https://player.webcamera.pl/index.php?id=ladekzdroj_cam_6dae56'),
(30, 2, 'Lidzbark - plaża NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=lidzbark_cam_2c458e'),
(31, 2, 'Sarbinowo - plaża live', 'https://player.webcamera.pl/index.php?id=sarbinowo_cam_bd6ec8'),
(32, 2, 'MIELNO - plaża', 'https://player.webcamera.pl/index.php?id=mielno_cam_0e2e7c'),
(33, 4, 'Orzysz miasto', 'https://player.webcamera.pl/index.php?id=orzysz2_cam_328e80'),
(34, 1, 'Zwardoń Ski', 'https://player.webcamera.pl/index.php?id=zwardon_cam_f2eb17'),
(35, 1, 'Panorama Tatr - ZĄB', 'https://player.webcamera.pl/index.php?id=willa_cam_922439'),
(36, 1, 'HARENDA na żywo', 'https://player.webcamera.pl/index.php?id=skywind_cam_62d9a1'),
(37, 1, 'Dziwnów na żywo', 'https://player.webcamera.pl/index.php?id=dziwnowum_cam_d0ed48'),
(38, 2, 'POBIEROWO plaża NOWOŚĆ z głosem', 'https://player.webcamera.pl/index.php?id=pobierowo_cam_89b36a'),
(39, 2, 'REWAL plaża NOWOŚĆ z głosem', 'https://player.webcamera.pl/index.php?id=gminarewal_cam_a882c3'),
(40, 4, 'Kazimierz Dolny na żywo', 'https://player.webcamera.pl/index.php?id=nocowanie_cam_0c89d6'),
(41, 1, 'MRZEŻYNO - port', 'https://player.webcamera.pl/index.php?id=mrzezyno_cam_8e2d5d'),
(42, 2, 'Niechorze plaża', 'https://player.webcamera.pl/index.php?id=rewal_cam_003f77'),
(43, 1, 'TRZĘSACZ plaża NOWOŚĆ z głosem', 'https://player.webcamera.pl/index.php?id=gminarewal_cam_635a3d'),
(44, 1, 'Ustka - Ławeczka Ireny Kwiatkowskiej', 'https://player.webcamera.pl/index.php?id=ustka_cam_e8679f'),
(51, 1, 'Widok na Polańczyk', 'https://player.webcamera.pl/index.php?id=polanczyk_cam_d1e2d8'),
(52, 4, 'OLSZTYN - panorama miasta', 'https://player.webcamera.pl/index.php?id=olsztyn_cam_43a997'),
(53, 1, 'Lidzbark - plaża NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=lidzbark_cam_2c458e'),
(54, 1, 'POBIEROWO plaża NOWOŚĆ z głosem', 'https://player.webcamera.pl/index.php?id=pobierowo_cam_89b36a'),
(55, 1, 'Kraków - Grodzka 2', 'https://player.webcamera.pl/index.php?id=krakowsan_cam_480f1a'),
(56, 1, 'Kraków-hotel Wentzl', 'https://player.webcamera.pl/index.php?id=wentzl_cam_be5c6c'),
(57, 1, 'Hejnał z Wieży Mariackiej codziennie o 12 obraz dostępny tylko na desktopach', 'https://www.webcamera.pl/mariacki/index.html?asd'),
(58, 1, 'Gdańsk - widok na Motławę', 'https://player.webcamera.pl/index.php?id=umgdansk_cam_11161c'),
(59, 5, ' Paśnik', 'https://player.webcamera.pl/index.php?id=babki_cam_628fc3'),
(61, 1, 'Krupówki środkowe live', 'https://player.webcamera.pl/index.php?id=zakopane_cam_cd5b2d'),
(62, 1, 'Polańczyk na żywo', 'https://player.webcamera.pl/index.php?id=polanczyk_cam_aa8511'),
(63, 5, 'Gniew - BOCIANY', 'https://player.webcamera.pl/index.php?id=sck_cam_1c646f'),
(64, 1, 'Gubałówka - Zakopane', 'https://player.webcamera.pl/index.php?id=pkl_cam_cc787e'),
(65, 1, 'Rzepiska - Tatry', 'https://player.webcamera.pl/index.php?id=airnet_cam_d60f3c'),
(66, 1, 'Szczawnica - deptak', 'https://player.webcamera.pl/index.php?id=nocowanie_cam_341030'),
(67, 1, 'Władysławowo-Półwysep', 'https://player.webcamera.pl/index.php?id=nocowanie_cam_6e8300'),
(68, 1, 'Widok na Polańczyk', 'https://player.webcamera.pl/index.php?id=polanczyk_cam_d1e2d8'),
(69, 1, 'Gubałówka - Zakopane', 'https://player.webcamera.pl/index.php?id=pkl_cam_f4e890'),
(70, 1, 'widok na Jezioro Solińskie NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=airnet_cam_0a7765'),
(71, 1, 'Darłowo Rynek NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=darlowo_cam_fd6665'),
(72, 1, 'Dziwnów na żywo', 'https://player.webcamera.pl/index.php?id=dziwnowum_cam_05f247'),
(73, 1, 'Karpacz - panorama miasta', 'https://player.webcamera.pl/index.php?id=karpacz_cam_ab594f'),
(74, 1, 'KRAKÓW widok na WAWEL', 'https://player.webcamera.pl/index.php?id=krakow_cam_4c3e37'),
(75, 2, 'Krynica Morska - port jachtowy', 'https://player.webcamera.pl/index.php?id=krynicamorska_cam_b92412'),
(76, 1, 'Międzywodzie plaża NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=miedzywodzie_cam_6b6996'),
(78, 1, 'Świnoujście - plaża live', 'https://player.webcamera.pl/index.php?id=swinoujscie_cam_7eca1e'),
(79, 1, 'USTKA Panorama', 'https://player.webcamera.pl/index.php?id=ustka_cam_4b0fca'),
(80, 1, 'Wisła Malinka - Skocznia Adama Małysza', 'https://player.webcamera.pl/index.php?id=wisla_cam_3d639c'),
(81, 1, 'WADOWICE - RYNEK', 'https://player.webcamera.pl/index.php?id=wadowice_cam_f87101'),
(82, 1, 'Wisła Malinka - Skocznia Adama Małysza', 'https://player.webcamera.pl/index.php?id=wisla_cam_21905c'),
(83, 2, 'ŁEBA plaża NOWOŚĆ', 'https://player.webcamera.pl/index.php?id=leba_cam_0d3c03'),
(85, 1, 'BUKOVINA Resort - widok na hotel', 'https://player.webcamera.pl/index.php?id=bukovina_cam_04f738'),
(87, 2, 'Ustronie Morskie - plaża', 'https://player.webcamera.pl/index.php?id=umustronie_cam_8305a4'),
(88, 1, 'Pustkowo', 'https://player.webcamera.pl/index.php?id=gminarewal_cam_95ef9b'),
(89, 1, 'Ustroń Czantoria na żywo', 'https://player.webcamera.pl/index.php?id=czantoria_cam_e3db1a'),
(90, 1, 'Ustroń Czantoria', 'https://player.webcamera.pl/index.php?id=czantoria_cam_beccf8'),
(92, 1, 'KOPA - górna stacja', 'https://player.webcamera.pl/index.php?id=kopa_cam_6ef73b');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `kategorie`
--
ALTER TABLE `kategorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `livevideo`
--
ALTER TABLE `livevideo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategorie_id` (`kategorie_id`);

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `livevideo`
--
ALTER TABLE `livevideo`
  ADD CONSTRAINT `livevideo_ibfk_1` FOREIGN KEY (`kategorie_id`) REFERENCES `kategorie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
