<?php

namespace Sda\LiveVideo\Template;

class Template {
    
    
    public function getIndexTemplate (array $video, $numberOfPages, $actualPage = 1 ) {
        $html = '<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                
                	<title>Kamera na żywo - Kamery HD z całej Polski i Świata - livevideo.pl</title>
        <meta name="description" content="Kamery w HD z całej Polski i Świata - Kamera na żywo - livevideo.pl">
        <meta name="keywords" content="Kamery HD z całej Polski na żywo. Kamery pogodowe ze stacji narciarskich,
        aktualne warunki narciarskie w Polsce, transmisje live, Kamery stoki,pogoda na jeziorach,  Mikołajki,a,Finlandia kamery,Norwegia kamery">
                
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <script src="js/jquery-3.1.1.min.js"></script>
                
                <link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
                <link rel="stylesheet" href="css/style.css">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
                <script src="http://vjs.zencdn.net/c/video.js"></script>
                <script src="js/jquery-3.2.1.min.js"></script>
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


            </head>
            <body style="background:Ivory">
            
         
                <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Strona główna</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
       
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kategorie <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li id="pierwszy"><a href="#">Góry</a></li>
            <li><a href="#">Morze</a></li>
            <li><a href="#">Miasta</a></li>
            <li><a href="#">Jeziora</a></li>
            <li><a href="#">Zwierzęta</a></li>
            <li><a href="#">Finlandia</a></li>
            <li><a href="#">Norwegia</a></li>
           </ul>
           
           <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Filmiki <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Fauna</a></li>
            <li><a href="#">Flora</a></li>
         
            
           </ul>
           
          <li class="Link"><a href="#">Nowości <span class="sr-only">(current)</span></a></li>
        </li>
      </ul>
      <script>
      $("#pierwszy").onclick(function() {
        alert("asd");
      });
      
</script>

      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="">
        </div>
        <button type="submit" class="btn btn-default">Wyszukaj</button>
        


      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Zaloguj się</a></li>
        <li><a href="#">Rejestracja</a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Oferta <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="http://www.webcommedia.pl/">webcommedia.pl</a></li>
            <li><a href="http://www.glosylasu.pl/">glosylasu.pl</a></li>
            <li><a href="http://www.watergames.pl/">watergames.pl</a></li>
           
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

			
	<div class="w3-content w3-section">

		<img class="mySlides w3-animate-fading" src="img/glosylasu.jpg" style="width:950px; height:120px;auto;clear:both">
		<img class="mySlides w3-animate-fading" src="img/waterlogo.jpg" style="width:950px; height:120px;auto;clear:both">
		<img class="mySlides w3-animate-fading" src="img/las4.jpg" style="width:950px; height:120px;auto;clear:both">
   
   
             </div>

                <script>
                var myIndex2 = 0;
                carousel2();

                function carousel2() {
                    var i;
                    var x = document.getElementsByClassName("mySlides");
                    for (i = 0; i < x.length; i++) {
                       x[i].style.display = "none";  
                    }
                    myIndex2++;
                    if (myIndex2 > x.length) {myIndex2 = 1}    
                    x[myIndex2-1].style.display = "block";  
                    setTimeout(carousel2, 5000);    
                }
                </script>

               
                
       </div>
    
        <div class="row" style="margin-left:50px;margin-right:0px;margin-bottom:0px">
	<div class="container">
        
		<h2 class="head" style="text-align:center;">Najnowszy portal z kamerami HD na Żywo z Miast, Plaż i Stacji Narciarskich</h2>
	</div>

            <div id="main" style="margin-left: 20px;overflow:hidden;">
  
            ';
            
      
 
        foreach ($video as $singleVideo){
            //var_dump($singleVideo);
            
            $html .= '<div style="border:0;width:300px;height:230px;float:left"> 
              <iframe src="' . $singleVideo['url'] . '" style="border:0;width:300px;height:200px;" allowfullscreen></iframe>
                <p>' . $singleVideo['name'] . '</p>  </div>
';
        }
        
       
         $html .= '</div>	
             

                     
                
	<div class="w3-content w3-section">
	  <img class="mySlides2" src="img/glosylasu.jpg" style="width:950px; height:120px;clear:both">
	  <img class="mySlides2" src="img/las6.jpg" style="width:950px; height:120px;auto;clear:both">
	  <img class="mySlides2" src="img/waterlogo.jpg" style="width:950px; height:120px;auto;clear:both">
	</div>


                <script>
                var myIndex = 0;
                carousel();

                function carousel() {
                    var i;
                    var x = document.getElementsByClassName("mySlides2");
                    for (i = 0; i < x.length; i++) {
                       x[i].style.display = "none";  
                    }
                    myIndex++;
                    if (myIndex > x.length) {myIndex = 1}    
                    x[myIndex-1].style.display = "block";  
                    setTimeout(carousel, 5000); // Change image every 5 seconds
                }
                </script>

                
                <nav aria-label="Page navigation" style="width:100%; margin:0 auto;text-align:center">
                <ul class="pagination">
                  ';
         
         for($i=1;$i<=$numberOfPages;$i++){
             if ($actualPage==$i){
                $html .= '<li><a href="index.php?page=' . $i . '">' . $i . '</a></li>';
             }else{
                $html .= '<li><a href="index.php?page=' . $i . '">' . $i . '</a></li>'; 
             }
             
         }
         
       
                  $html .= '
                   
             
                 </ul>
              </nav>
              </div>

              
                 <footer class="footer-distributed">

			<div class="footer-right">

				
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-linkedin"></i></a>
				<a href="#"><i class="fa fa-github"></i></a>

			</div>

			<div class="footer-left">

				<p class="footer-links">
					<a href="#">Strona główna</a>
					·
					<a href="#">O serwisie</a>
					·
					<a href="#">Regulamin</a>
					·
					<a href="#">O nas</a>
					·
					<a href="#">Polityka Prywatności</a>
					·
					<a href="#">Kontakt</a>
				</p>

				<p>Copyright © by livevideo.pl Wszelkie Prawa Zastrzeżone</p>
			</div>

		</footer>
                
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
                <script src="http://vjs.zencdn.net/c/video.js"></script>
                <script src="js/jquery-3.2.1.min.js"></script>
                <script src="js/Main.js"></script>

</body>
        
            </html>';
        
        echo $html;
    }    
    
  
 }
 
 

 