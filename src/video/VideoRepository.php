<?php

namespace Sda\LiveVideo\Video;

use Doctrine\DBAL\Connection;

class VideoRepository
{

    const VIDEO_PER_PAGE = 20;
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * VideoRepository constructor.
     * @param Connection $dbh
     */
    public function __construct(Connection $dbh)
    {
        $this->dbh = $dbh;
    }

    public function getAllVideos()
    {
        $sth = $this->dbh->prepare('SELECT * FROM `livevideo`');
        $sth->execute();
        $videoData = $sth->fetchAll();

        return $videoData;
    }

    public function getKategria($id){

    $sth = $this->dbh->prepare('SELECT * FROM `livevideo` WHERE `kategorie_id` = :id');
    $sth->bindValue('id',$id, \PDO::PARAM_INT);
    $sth->execute();
    $videoDataKat = $sth->fetchAll();

    return $videoDataKat;
}



    public function countAllVideos()
    {
        $sth = $this->dbh->prepare('SELECT COUNT(*) FROM `livevideo`');
        $sth->execute();
        $videoData = $sth->fetchColumn();

        return $videoData;
    }

    public function getVideosByPage($page = 1)
    {

        $offset = $page * self::VIDEO_PER_PAGE - self::VIDEO_PER_PAGE;

        $sth = $this->dbh->prepare('SELECT * FROM `livevideo` LIMIT :offset, :limit');
        $sth->bindValue('offset', $offset, \PDO::PARAM_INT);
        $sth->bindValue('limit', self::VIDEO_PER_PAGE, \PDO::PARAM_INT);
        $sth->execute();
        $videoData = $sth->fetchAll();

        return $videoData;
    }

    public function getVideoById($id)
    {
        $sth = $this->dbh->prepare('SELECT * FROM `livevideo` WHERE `id` =:id');
        $sth->bindValue('id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $videoData = $sth->fetch();

        return $videoData;

    }

    private function login()
    {
        unset($_SESSION['registeredEmail']);
        //$template->
        if (array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {

            $user = new User(
                htmlspecialchars($_POST['login']),
                htmlspecialchars($_POST['password']),
                $this->dbh
            );

            if ($user->validateUser() === false) {
                echo 'Niepoprawny login lub hasło<br>';
            } else {
                $_SESSION['login'] = substr($user->getEmail(), 0, strpos($user->getEmail(), '@'));

                header('Location: index.php?action=account');
            }
        }
        //$template->show();
        echo
        '<!DOCTYPE html>
		<html lang="pl">
		<head>
			<meta charset="UTF-8">
			<title>Logowanie</title>
		</head>
		<body>
			<form action="index.php?action=login" method="POST">
				<label for="login">Login </label><input type="text" name="login" id="login" value=""><br>
				<label for="password">Hasło </label><input type="password" name="password" id="password"><br><br>
				<button>Wyślij!</button>
			</form>
			<div>
				<p>Nie masz konta?</p>
				<a href="index.php?action=register">Zarejestruj się!</a>
			</div>
		</body>
		</html>';
    }

    private function account()
    {
        if (array_key_exists('login', $_SESSION)) {

            echo 'Witaj ' . $_SESSION['login'] . ', jesteś zalogowany!';
            echo '<br>';

            echo
            '<div style="float: right;">
					<a href="index.php?action=logout">Wyloguj się.</a>

			</div>';
        } else {
            header('Location: index.php?action=login');
        }
    }

    private function register()
    {
        if (array_key_exists('login', $_POST) && array_key_exists('password', $_POST)) {

            $registerUser = new RegisterUser(
                htmlspecialchars($_POST['login']),
                htmlspecialchars($_POST['password']),
                $this->dbh
            );

            if ($registerUser->loginAlreadyExists() === true) {
                echo 'Podany email jest zajęty.';
                //header('Location: index.php?action=register');
            } elseif ($registerUser->addUser() === true) {
                $_SESSION['registeredEmail'] = $registerUser->getEmail();
                header('Location: index.php?action=registered');
            } else {
                echo 'Niepoprawny login lub hasło<br>';
            }
        }

        echo
        '<form action="index.php?action=register" method="POST">
			<label for="login">Stwórz login </label><input type="text" name="login" id="login" value=""><br>
			<label for="password">Stwórz hasło </label><input type="password" name="password" id="password"><br><br>
			<button>Zarejestruj!</button>
		</form>';
    }

    private function registered()
    {
        if (array_key_exists('registeredEmail', $_SESSION)) {
            echo 'Pomyślnie zarejestrowano!';
            echo
            '<div>
					<a href="index.php?action=login">Przejdź do strony logowania.</a>
			</div>';
        } else {
            header('Location: index.php?action=register');
        }
    }

    private function logout()
    {
        session_destroy();
        header('Location: index.php?action=login');
    }


    public function addUser(User $user)
    {

        $sth = $this->dbh->prepare('SELECT * FROM `users` WHERE `email` = :email');
        $sth->bindValue('email', $user->getEmail(), \PDO::PARAM_INT);
        $sth->execute();
        $userData = $sth->fetch();
        if (false === $userData) {
            return $this->dbh->insert('users', ['email' => $user->getEmail(), 'password' => $user->getPassword()]);
        } else {
            echo 'uzytkownik o podaym loginie istnieje';
        }


    }
}



