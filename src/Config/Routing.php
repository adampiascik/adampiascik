<?php

namespace Sda\LiveVideo\Config;

class Routing {

    const INDEX = 'index';
    const CAMERA_LIST = 'videoList';
    const CAMERA = 'video';
    
}