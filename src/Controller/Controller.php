<?php

namespace Sda\LiveVideo\Controller;


use Doctrine\DBAL\Connection;
use Sda\LiveVideo\User\User;
use Sda\LiveVideo\User\RegisterUser;
use Sda\LiveVideo\Config\Routing;
use Sda\LiveVideo\Template\Template;
use Sda\LiveVideo\Video\VideoRepository;
use Sda\LiveVideo\Request\Request;
use Sda\LiveVideo\Response\Response;

class Controller {
    
    private $request;
    private $response;
    private $template;
    private $repo;
    
    public function __construct(
            Request $request, 
            Response $response, 
            Template $template, 
            VideoRepository $repo
            ) {
        $this->request = $request;
        $this->response = $response;
        $this->template = $template;
        $this->repo = $repo;
    }



    public function run(){

          $action = array_key_exists('action', $_GET) ? $_GET['action'] : 'index' ;

          switch ($action) {
               case Routing::INDEX;
                   $this->index();
                   break;
               case Routing::Video_LIST:
                   $this->ajaxGetVideoList();
                   break;              
               case Routing::Video:
                   $this->ajaxGetVideo();
                   break;
               default:
                   echo "404 - Strona nie znaleziona";
                   break;
            }
      
   }

  private function ajaxGetVideoList() {
      
       $video = $this->repo->getAllVideos();
//       $video = $this->repo->getKategria("2");
//       var_dump($video);

      echo '<ul>';    
      foreach ($video as $movie) {
          
            //echo   '<li>'. $movie['name']    .'</li>';
       echo "<li>
           <p>". $movie['name'] ."</p>
           <a href='index.php?action=video&videoId=". $movie['id'] ."'><img src='". $movie['thumbnail'] ."' /></a>
           <a href='". $movie['url'] ."'>". $movie['url'] ."</a>
          
           </li>";
       }
       echo '</ul>';
          
              
   }
  
   public function ajaxGetVideo(){
       $id = $_GET['videoId'];
       $this->repo->getMovieById($id);
   }


  private function index(){
      $page = $this->request->getParamFromGet('page', 1);
      
      $totalVideo = $this->repo->countAllVideos();
      
      $numberOfPages = ceil($totalVideo / VideoRepository::VIDEO_PER_PAGE);
      
       $video = $this->repo->getVideosByPage($page);
       $video2 = $this->repo->getKategria("2");
     
       echo $this->template->getIndexTemplate($video, $numberOfPages, $page);
      
    }

}




	