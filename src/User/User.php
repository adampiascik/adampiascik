<?php

namespace Sda\LiveVideo\User;

use Doctrine\DBAL\Connection;

class User {
	
	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $pass;
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * User constructor.
     * @param string $email
     * @param string $pass
     * @param Connection $dbh
     */
    public function __construct($email, $pass, Connection $dbh) {
		$this->email = $email;
		$this->pass = $pass;
        $this->dbh = $dbh;
    }

	/**
	 * @return mixed
	 */
	// public function validateEmail() {
	// 	return strpos($this->email, '@');
	// }

	/**
	 * @return bool
	 */
	public function validateUser() {

	    $sth = $this->dbh->prepare('SELECT login, password FROM `users` WHERE `login` = :moj_login');
        $sth->bindValue('moj_login', $this->getEmail(), \PDO::PARAM_STR);
        $sth->execute();

        $userDbData = $sth->fetch();

        if(false !== $userDbData &&
            hash_equals($userDbData['password'], crypt($this->getPass(), $userDbData['password']))){
                echo 'Password verified!';
                return true;
        }
		return false;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getPass() {
		return $this->pass;
	}
}