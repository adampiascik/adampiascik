<?php

namespace Sda\LiveVideo\User;

use Doctrine\DBAL\Connection;

class RegisterUser {
	
	/**
 * @var string
 */
    private $email;

    /**
     * @var string
     */
    private $pass;
    /**
     * @var Connection
     */
    private $dbh;

    /**
     * RegisterUser constructor.
     * @param string $email
     * @param string $pass
     * @param Connection $dbh
     */
    public function __construct($email, $pass, Connection $dbh) {
        $this->email = $email;
        $this->pass = $pass;
        $this->dbh = $dbh;
    }

    /**
     * @return bool
     */
    public function loginAlreadyExists() {
        $sth = $this->dbh->prepare('SELECT login, password FROM `users` WHERE `login` = :moj_login');
        $sth->bindValue('moj_login', $this->getEmail(), \PDO::PARAM_STR);
        $sth->execute();

        $userDbData = $sth->fetch();

        return false !== $userDbData;
	}

    /**
     * @return bool
     */
    public function addUser() {
		if (strpos($_POST['login'], '@') !== false && isset($_POST['password']) === true) {
            $hashedPassword = crypt($this->getPass(), uniqid('', true));

            $dataToInsert = [
                'login' => $this->getEmail(),
                'password' => $hashedPassword
            ];

            $lastInsertId = $this->dbh->insert('users', $dataToInsert);

			return true;
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

    /**
     * @return string
     */
    public function getPass() {
		return $this->pass;
	}
}

