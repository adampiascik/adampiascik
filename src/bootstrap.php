<?php

use Doctrine\DBAL\Configuration;

use Doctrine\DBAL\DriverManager;

use Sda\LiveVideo\Request\Request;
use Sda\LiveVideo\Config\Config;
use Sda\LiveVideo\Response\Response;
use Sda\LiveVideo\Template\Template;
use Sda\LiveVideo\Controller\Controller;
use Sda\LiveVideo\Video\VideoRepository;



require_once __DIR__ . '/../vendor/autoload.php';



$config = new Configuration();
$request = new Request();
$response = new Response();
$template = new Template();



$dbh = DriverManager::getConnection(Config::DB_CONNECTION_DATA, $config);
$rep = new VideoRepository($dbh);
$app = new Controller($request, $response, $template, $rep);
$app->run();

